// To get the value from place holder
var data = document.getElementById("stringSearch").value.toLowerCase();
invoice_header = ["Product name", "Category", "MRP", "Quantity", "Price"]
var txt = "Rahul";
var name = "Name"
var min = 0;
var max = 0;
var items = [];

function valueChanged(){
    if(document.getElementById("is_Category").checked == true)
    {
        document.getElementById("is_Name").checked = false;
        document.getElementById("is_Range").checked = false;
        var choice = prompt("Please enter category: fruits or vegetables or dairy or biscuits", "");
        if (choice == null || choice == "") {
        txt = "User cancelled the prompt.";
        }
        else {
        txt = choice;
        }
    }
    if (document.getElementById("is_Range").checked == true)
    {
        document.getElementById("is_Category").checked = false;
        document.getElementById("is_Name").checked = false;
        min = prompt("Please enter minimum value", "");
        max = prompt("Please enter maxium value", "");
        if (min == 0 && max == 0)
        {
            document.getElementById("table_div").innerHTML = "<center><h1>No such Item found !!</h1></center>";
        }
    }
    if (document.getElementById("is_Name").checked == true) {
        document.getElementById("is_Range").checked = false;
        document.getElementById("is_Category").checked = false;
        name = "Rahul";
    }
}

var fruits = [{
    name: "Mango",
    category: "Fruit",
    MRP: 20
},
{
    name: "Blueberry",
    category: "Fruit",
    MRP: 40
},
{
    name: "Banana",
    category: "Fruit",
    MRP: 15
},
{
    name: "Apple",
    category: "Fruit",
    MRP: 60
},
{
    name: "Grapes",
    category: "Fruit",
    MRP: 40
},
{
    name: "Litchi",
    category: "Fruit",
    MRP: 120
},
{
    name: "Guava",
    category: "Fruit",
    MRP: 20
},
{
    name: "Apricot",
    category: "Fruit",
    MRP: 140
},
{
    name: "Blackberry",
    category: "Fruit",
    MRP: 65
},
{
    name: "Orange",
    category: "Fruit",
    MRP: 55
},]
var vegetables = [{
    name: "Beetroot",
    category: "Vagetable",
    MRP: 10
},
{
    name: "Bittermelon",
    category: "Vagetable",
    MRP: 40
},
{
    name: "Pumpkin",
    category: "Vagetable",
    MRP: 50
},
{
    name: "Cabbage",
    category: "Vagetable",
    MRP: 80
},
{
    name: "Carrot",
    category: "Vagetable",
    MRP: 32
},
{
    name: "Bittermelon",
    category: "Vagetable",
    MRP: 15
},
{
    name: "Coriander Leaf",
    category: "Vagetable",
    MRP: 180
},
{
    name: "Cucumber",
    category: "Vagetable",
    MRP: 70
},
{
    name: "Brinjal",
    category: "Vagetable",
    MRP: 45
},
{
    name: "Onion",
    category: "Vagetable",
    MRP: 55
},]
var dairy = [{
    name: "Basundi",
    category: "Dairy",
    MRP: 45
},
{
    name: "Butter",
    category: "Dairy",
    MRP: 30
},
{
    name: "Cheese",
    category: "Dairy",
    MRP: 68
},
{
    name: "Milk",
    category: "Dairy",
    MRP: 54
},
{
    name: "Clotted cream",
    category: "Dairy",
    MRP: 115
},
{
    name: "Cream cheese",
    category: "Dairy",
    MRP: 180
},
{
    name: "Curd",
    category: "Dairy",
    MRP: 90
},
{
    name: "Custard",
    category: "Dairy",
    MRP: 150
},
{
    name: "Khoa",
    category: "Dairy",
    MRP: 225
},
{
    name: "Kulfi",
    category: "Dairy",
    MRP: 65
},]   
var biscuits = [{
    name: "Parle G",
    category: "Biscuit",
    MRP: 10
},
{
    name: "Krackjack",
    category: "Biscuit",
    MRP: 20
},
{
    name: "Happy Happy",
    category: "Biscuit",
    MRP: 20
},
{
    name: "Festo",
    category: "Biscuit",
    MRP: 60
},
{
    name: "Marie gold",
    category: "Biscuit",
    MRP: 25
},
{
    name: "Good Day",
    category: "Biscuit",
    MRP: 20
},
{
    name: "Little Hearts",
    category: "Biscuit",
    MRP: 40
},
{
    name: "Butter Bake",
    category: "Biscuit",
    MRP: 90
},
{
    name: "Horlicks",
    category: "Biscuit",
    MRP: 30
},
{
    name: "Dream Cream",
    category: "Biscuit",
    MRP: 55
},]


var SearchFunction = () => {
    var data = document.getElementById("stringSearch").value.toLowerCase();
    var regexp = new RegExp (data, "g");
    var count = 0;
    var table = "<tr><th>Name</th><th>Category</th><th>Price</th><th>Quantity</th></tr>";

    // Extract all names that matches
    if (min == null && max == null) {
        document.getElementById("table_div").innerHTML = "<center><h1>No such Item found !!</h1></center>";
    }
    if (min >= 0 && max > 0) {
        for(var item = 0; item < fruits.length; item ++ ) {
            if (fruits[item].name.toLowerCase().match(regexp) != null && fruits[item].MRP >= min && fruits[item].MRP <= max) {
                table = table + "<tr><td>" + fruits[item].name + "</td><td>" + fruits[item].category + "</td><td>" + fruits[item].MRP + 
                "</td><td><input type='number' value = '0' min='0' max='20' id='num1" + item + "'></td></tr>";
                count++;
            }
        }
        for(var item = 0; item < vegetables.length; item ++ ) {
            if (vegetables[item].name.toLowerCase().match(regexp) != null && vegetables[item].MRP >= min && vegetables[item].MRP <= max) {
                table = table + "<tr><td>" + vegetables[item].name + "</td><td>" + vegetables[item].category + "</td><td>" + 
                vegetables[item].MRP + "</td><td><input type='number' value = '0' min='0' max='20' id='num2" + item + "'></td></tr>";
                count++;
            }
        }   
        for(var item = 0; item < dairy.length; item ++ ) {
            if (dairy[item].name.toLowerCase().match(regexp) != null && dairy[item].MRP >= min && dairy[item].MRP <= max) {
                table = table + "<tr><td>" + dairy[item].name + "</td><td>" + dairy[item].category + "</td><td>" + dairy[item].MRP + 
                "</td><td><input type='number' value = '0' min='0' max='20' id='num3" + item + "'></td></tr>";
                count++;
            }
        } 
        for(var item = 0; item < biscuits.length; item ++ ) {
            if (biscuits[item].name.toLowerCase().match(regexp) != null && biscuits[item].MRP >= min && biscuits[item].MRP <= max) {
                table = table + "<tr><td>" + biscuits[item].name + "</td><td>" + biscuits[item].category + "</td><td>" + biscuits[item].MRP + 
                "</td><td><input type='number' value = '0' min='0' max='20' id='num4" + item + "'></td></tr>";
                count++;
            }
        }
    }
    if (txt == "fruits" || txt == "vegetables" || txt == "dairy" || txt == "biscuits")
    {
        if(table != " ")
        {
        switch (txt) 
        {
            case "fruits":
            for(var item = 0; item < fruits.length; item ++ ) {
                if (fruits[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + fruits[item].name + "</td><td>" + fruits[item].category + "</td><td>" + fruits[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num1" + item + "'></td></tr>";
                    count++;
                }
            }
            break;
            case "vegetables":
            for(var item = 0; item < vegetables.length; item ++ ) {
                if (vegetables[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + vegetables[item].name + "</td><td>" + vegetables[item].category + "</td><td>" + vegetables[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num2" + item + "'></td></tr>";
                    count++;
                }
            }    
            break;    
            case "dairy":
            for(var item = 0; item < dairy.length; item ++ ) {
                if (dairy[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + dairy[item].name + "</td><td>" + dairy[item].category + "</td><td>" + dairy[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num3" + item + "'></td></tr>";
                    count++;
                }
            }
            break;
            case "biscuits":
            for(var item = 0; item < biscuits.length; item ++ ) {
                if (biscuits[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + biscuits[item].name + "</td><td>" + biscuits[item].category + "</td><td>" + biscuits[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num4" + item + "'></td></tr>";
                    count++;
                }
            }
            break;
            default:
            document.getElementById("table_div").innerHTML = "<center><h1>No such Item found !!!!!</h1></center>";
        } 
    } 
    else {
        switch (txt) 
        {
            case "fruits":
            for(var item = 0; item < fruits.length; item ++ ) {
                if (fruits[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + fruits[item].name + "</td><td>" + fruits[item].category + "</td><td>" + fruits[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num1" + item + "'></td></tr>";
                    count++;
                }
            }
            break;
            case "vegetables":
            for(var item = 0; item < vegetables.length; item ++ ) {
                if (vegetables[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + vegetables[item].name + "</td><td>" + vegetables[item].category + "</td><td>" + vegetables[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num2" + item + "'></td></tr>";
                    count++;
                }
            }    
            break;    
            case "dairy":
            for(var item = 0; item < dairy.length; item ++ ) {
                if (dairy[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + dairy[item].name + "</td><td>" + dairy[item].category + "</td><td>" + dairy[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num3" + item + "'></td></tr>";
                    count++;
                }
            }
            break;
            case "biscuits":
            for(var item = 0; item < biscuits.length; item ++ ) {
                if (biscuits[item].name.toLowerCase().match(regexp) != null) {
                    table = table + "<tr><td>" + biscuits[item].name + "</td><td>" + biscuits[item].category + "</td><td>" + biscuits[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num4" + item + "'></td></tr>";
                    count++;
                }
            }
            break;
            default:
            document.getElementById("table_div").innerHTML = "<center><h1>No such Item found !!!!!</h1></center>";
        } 
    } 
}
    if (name == "Rahul")
    {
        for(var item = 0; item < fruits.length; item ++ ) {
            if (fruits[item].name.toLowerCase().match(regexp) != null) {
                table = table + "<tr><td>" + fruits[item].name + "</td><td>" + fruits[item].category + "</td><td>" + fruits[item].MRP + 
                    "</td><td><input type='number' value = '0' min='0' max='20' id='num1" + item + "'></td></tr>";
                    count++;
            }
        }
        for(var item = 0; item < vegetables.length; item ++ ) {
            if (vegetables[item].name.toLowerCase().match(regexp) != null) {
                table = table + "<tr><td>" + vegetables[item].name + "</td><td>" + vegetables[item].category + "</td><td>" +
                vegetables[item].MRP + "</td><td><input type='number' value = '0' min='0' max='20' id='num2" + item + "'></td></tr>";
                count++;
            }
        }
        for(var item = 0; item < dairy.length; item ++ ) {
            if (dairy[item].name.toLowerCase().match(regexp) != null) {
                table = table + "<tr><td>" + dairy[item].name + "</td><td>" + dairy[item].category + "</td><td>" + dairy[item].MRP + 
                "</td><td><input type='number' value = '0' min='0' max='20' id='num3" + item + "'></td></tr>";
                count++;
            }
        }   
        for(var item = 0; item < biscuits.length; item ++ ) {
            if (biscuits[item].name.toLowerCase().match(regexp) != null) {
                table = table + "<tr><td>" + biscuits[item].name + "</td><td>" + biscuits[item].category + "</td><td>" + biscuits[item].MRP + 
                "</td><td><input type='number' value = '0' min='0' max='20' id='num4" + item + "'></td></tr>";
                count++;
            }
        }
    }
    if (count < 1)
    {
        document.getElementById("table_div").innerHTML = "<center><h1>No such Item found !!!!! </h1></center>";    
    }
    document.getElementById("table-data").innerHTML = table;
    document.getElementById("table-data").setAttribute("border", "2");
}

var AddCart = () => 
{
    items.length = 0;
    var data = document.getElementById("stringSearch").value.toLowerCase();
    var regexp = new RegExp (data, "g");


    if (min >= 0 && max > 0) {
        for(var item = 0; item < fruits.length; item ++ ) {
            if (fruits[item].name.toLowerCase().match(regexp) != null && fruits[item].MRP >= min && fruits[item].MRP <= max) {
                fruits[item].quantity = document.getElementById("num1" + item).value;
                    // value updation in JSON
                }
                if(fruits[item].quantity > 0) {
                    items.push([fruits[item].name, fruits[item].category, fruits[item].MRP, fruits[item].quantity, 
                    fruits[item].MRP * fruits[item].quantity]);
                    // storing data in array
            }
        }
        for(var item = 0; item < vegetables.length; item ++ ) {
            if (vegetables[item].name.toLowerCase().match(regexp) != null && vegetables[item].MRP >= min && vegetables[item].MRP <= max) {
                vegetables[item].quantity = document.getElementById("num2" + item).value;
                    // value updation in JSON
                }
                if(vegetables[item].quantity > 0) {
                    items.push([vegetables[item].name, vegetables[item].category, vegetables[item].MRP, vegetables[item].quantity,
                        vegetables[item].MRP * vegetables[item].quantity]);
                    // storing data in array
            }
        }   
        for(var item = 0; item < dairy.length; item ++ ) {
            if (dairy[item].name.toLowerCase().match(regexp) != null && dairy[item].MRP >= min && dairy[item].MRP <= max) {
                dairy[item].quantity = document.getElementById("num3" + item).value;
                    // value updation in JSON
                }
                if(dairy[item].quantity > 0) {
                    items.push([dairy[item].name, dairy[item].category, dairy[item].MRP, dairy[item].quantity,
                    dairy[item].MRP * dairy[item].quantity]);
                    // storing data in array
            }
        } 
        for(var item = 0; item < biscuits.length; item ++ ) {
            if (biscuits[item].name.toLowerCase().match(regexp) != null && biscuits[item].MRP >= min && biscuits[item].MRP <= max) {
                biscuits[item].quantity = document.getElementById("num4" + item).value;
                    // value updation in JSON
                }
                if(biscuits[item].quantity > 0) {
                    items.push([biscuits[item].name, biscuits[item].category, biscuits[item].MRP, biscuits[item].quantity,
                    biscuits[item].MRP * biscuits[item].quantity]);
                    // storing data in array
            }
        }
    }

    if (txt == "fruits" || txt == "vegetables" || txt == "dairy" || txt == "biscuits")
    {
        switch (txt) 
        {
            case "fruits":
            for(var item = 0; item < fruits.length; item ++ ) {
                if (fruits[item].name.toLowerCase().match(regexp) != null) {
                    fruits[item].quantity = document.getElementById("num1" + item).value;
                    // value updation in JSON
                }
                if(fruits[item].quantity > 0) {
                    items.push([fruits[item].name, fruits[item].category, fruits[item].MRP, fruits[item].quantity, 
                    fruits[item].MRP * fruits[item].quantity]);
                    // storing data in array
        
                }
            }
            break;
            case "vegetables":
            for(var item = 0; item < vegetables.length; item ++ ) {
                if (vegetables[item].name.toLowerCase().match(regexp) != null) {
                    vegetables[item].quantity = document.getElementById("num2" + item).value;
                    // value updation in JSON
                }
                if(vegetables[item].quantity > 0) {
                    items.push([vegetables[item].name, vegetables[item].category, vegetables[item].MRP, vegetables[item].quantity,
                        vegetables[item].MRP * vegetables[item].quantity]);
                    // storing data in array
                }
            }    
            break;    
            case "dairy":
            for(var item = 0; item < dairy.length; item ++ ) {
                if (dairy[item].name.toLowerCase().match(regexp) != null) {
                    dairy[item].quantity = document.getElementById("num3" + item).value;
                    // value updation in JSON
                }
                if(dairy[item].quantity > 0) {
                    items.push([dairy[item].name, dairy[item].category, dairy[item].MRP, dairy[item].quantity,
                    dairy[item].MRP * dairy[item].quantity]);
                    // storing data in array
                }
            }
            break;
            case "biscuits":
            for(var item = 0; item < biscuits.length; item ++ ) {
                if (biscuits[item].name.toLowerCase().match(regexp) != null) {
                    biscuits[item].quantity = document.getElementById("num4" + item).value;
                    // value updation in JSON
                }
                if(biscuits[item].quantity > 0) {
                    items.push([biscuits[item].name, biscuits[item].category, biscuits[item].MRP, biscuits[item].quantity,
                    biscuits[item].MRP * biscuits[item].quantity]);
                    // storing data in array
                }
            }
            break;
            default:
            Console.log("My name is Rahul Maheshwari")
        } 
    }
    if (name == "Rahul") {
        for(var item = 0; item < fruits.length; item ++ ) {
            if (fruits[item].name.toLowerCase().match(regexp) != null) {
                fruits[item].quantity = document.getElementById("num1" + item).value;
                // value updation in JSON
            }
            if(fruits[item].quantity > 0) {
                items.push([fruits[item].name, fruits[item].category, fruits[item].MRP, fruits[item].quantity, 
                fruits[item].MRP * fruits[item].quantity]);
                // storing data in array
    
            }
        }
        for(var item = 0; item < vegetables.length; item ++ ) {
            if (vegetables[item].name.toLowerCase().match(regexp) != null) {
                vegetables[item].quantity = document.getElementById("num2" + item).value;
                // value updation in JSON
            }
            if(vegetables[item].quantity > 0) {
                items.push([vegetables[item].name, vegetables[item].category, vegetables[item].MRP, vegetables[item].quantity,
                    vegetables[item].MRP * vegetables[item].quantity]);
                // storing data in array
            }
        }
        for(var item = 0; item < dairy.length; item ++ ) {
            if (dairy[item].name.toLowerCase().match(regexp) != null) {
                dairy[item].quantity = document.getElementById("num3" + item).value;
                // value updation in JSON
            }
            if(dairy[item].quantity > 0) {
                items.push([dairy[item].name, dairy[item].category, dairy[item].MRP, dairy[item].quantity,
                dairy[item].MRP * dairy[item].quantity]);
                // storing data in array
            }
        }
        for(var item = 0; item < biscuits.length; item ++ ) {
            if (biscuits[item].name.toLowerCase().match(regexp) != null) {
                biscuits[item].quantity = document.getElementById("num4" + item).value;
                // value updation in JSON
            }
            if(biscuits[item].quantity > 0) {
                items.push([biscuits[item].name, biscuits[item].category, biscuits[item].MRP, biscuits[item].quantity,
                biscuits[item].MRP * biscuits[item].quantity]);
                // storing data in array
            }
        }
    } 
    GenerateCartTable();
}

function GenerateCartTable() {
    
    var cart_table = "<tr><th>Name</th><th>Quantity</th></tr>";
    for (var i = 0; i < items.length; i++) {
        cart_table = cart_table + "<tr><td>" + items[i][0] + "</td><td>" + items[i][3] + "</td></tr>";
        }
    document.getElementById("cart-table-data").innerHTML = cart_table;
    document.getElementById("cart-table-data").setAttribute("border", "2");
}


var Reset = () => {
    document.getElementById("table-data").innerHTML = "";
    document.getElementById("stringSearch").value = "";
    for(var item = 0; item < fruits.length; item ++ ) {
        fruits[item].quantity = 0;
    }
    for(var item = 0; item < vegetables.length; item ++ ) {
        vegetables[item].quantity = 0;
    }
    for(var item = 0; item < dairy.length; item ++ ) {
        dairy[item].quantity = 0;
    }
    for(var item = 0; item < biscuits.length; item ++ ) {
        biscuits[item].quantity = 0;
    }
} 

// this function is used to create the invoice table in existing div by delete all the previous data
// and to create invoice table so appendChild is used
// .innerHtml method was giving error
function GenerateInvoiceTable() {
    
    //Create a HTML Table element.
    var table = document.createElement("table");
    table.setAttribute("id", "invoice-table-data");
 
    //Get the count of columns.
    var columnCount = items[0].length;
 
    //Add the header row.
    var row = table.insertRow(-1);
    for (var i = 0; i < columnCount; i++) {
        var headerCell = document.createElement("th");
        headerCell.innerHTML = invoice_header[i];
        row.appendChild(headerCell);
    }
    var sum = 0;
    for (var i = 0; i < items.length; i++) {
        sum = sum + items[i][4];
    }

    //Add the data rows.
    for (var i = 0; i <= items.length; i++) {
        row = table.insertRow(-1);
        if(i!=items.length){
        for (var j = 0; j < columnCount; j++) {
            var cell = row.insertCell(-1);
            cell.innerHTML = items[i][j]; }}
        if (i==items.length) {
            var cell = row.insertCell(-1);
            cell.colSpan = "5";
            cell.innerHTML = "<h2><b> Total Amount to be paid = " + sum + "</b></h2>" ; 
        }
    }
    
    //table = table + "<tr><td colspan='4'>" + sum + "</td></tr>";  
 
    var table_div = document.getElementById("invoice");
    table_div.innerHTML = "<center><h1>Purchase receipt</h1></center><br/>";
    table_div.appendChild(table);
}













/*function GenerateInvoiceTable() {
    
    var invoice_table = "<tr><th>Name</th><th>Category</th><th>MRP</th><th>Quantity</th><th>Price</th></tr>";
    for (var i = 0; i < items.length; i++) {
        invoice_table = invoice_table + "<tr><td>" + items[i][0] + "</td><td>" + items[i][1] + "</td><td>" + items[i][2] + 
        "</td><td>" + items[i][3] + "</td><td>" + items[i][4] +"</td></tr>";
        }
    var sum = 0;
    for (var i = 0; i < items.length; i++) {
        sum = sum + items[i][4];
    }
    var div = document.getElementById("invoice");
    div.innerHTML = "";
    document.getElementById("invoice-table-data").innerHTML = invoice_table;
    document.getElementById("invoice-table-data").setAttribute("border", "2");
}*/

/*var CheckOut = () => {
    
    var invoice_table = "<tr><th>Name</th><th>Category</th><th>MRP</th><th>Quantity</th><th>Price</th></tr>";
    for(var item = 0; item < fruits.length; item ++ ) {
        if(fruits[item].quantity > 0) {
            invoice_table = invoice_table + "<tr><td>" + fruits[item].name + "</td><td>" + fruits[item].category + "</td><td>" 
            + fruits[item].MRP + "</td><td>" + fruits[item].quantity + "</td><td>" + fruits[item].MRP * fruits[item].quantity +"</td></tr>";
            count++;
        }
        if(vegetables[item].quantity > 0) {
            invoice_table = invoice_table + "<tr><td>" + vegetables[item].name + "</td><td>" + vegetables[item].category + "</td><td>" 
            + vegetables[item].MRP + "</td><td>" + vegetables[item].quantity + "</td><td>" + vegetables[item].MRP * vegetables[item].quantity
            +"</td></tr>";
            count++;
        }
        if(dairy[item].quantity > 0) {
            invoice_table = invoice_table + "<tr><td>" + dairy[item].name + "</td><td>" + dairy[item].category + "</td><td>" 
            + dairy[item].MRP + "</td><td>" + dairy[item].quantity + "</td><td>" + dairy[item].MRP * dairy[item].quantity +"</td></tr>";
            count;
        }
        if(biscuits[item].quantity > 0) {
            invoice_table = invoice_table + "<tr><td>" + biscuits[item].name + "</td><td>" + biscuits[item].category + "</td><td>" 
            + biscuits[item].MRP + "</td><td>" + biscuits[item].quantity + "</td><td>" + biscuits[item].MRP * biscuits[item].quantity
            +"</td></tr>";
            count++;
        }
   
        document.getElementById("invoice").innerHTML = "";
    }

}

if (count > 0) {
    document.getElementById("invoice-table-data").innerHTML = invoice_table;
    document.getElementById("invoice-table-data").setAttribute("border", "2");
    window.location.href = "invoice.html";
    
    
}
else {
    var str = "<br/><h2> You did not selected anything </h2><br/><br/><h3>Please buy something then press Check Out</h3><br/>";
    var warning = str.fontcolor("red");
    document.getElementById("table-data").innerHTML = warning;
}*/